package main

import (
	"machine"
	"time"
)

type LED struct {
	pin machine.Pin
}

func NewLED(pin machine.Pin) *LED {

	pin.Configure(machine.PinConfig{Mode: machine.PinOutput})

	return &LED{pin}
}

func (l *LED) Blink(t time.Duration) {
	start := time.Now()
	for start.Add(t).After(time.Now()) {
		l.pin.High()
		time.Sleep(time.Millisecond * 100)
		l.pin.Low()
		time.Sleep(time.Millisecond * 100)
	}
	l.pin.Low()
	return
}
