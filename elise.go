package main

import (
	"time"
	"tinygo.org/x/drivers/buzzer"
)

func play(bzr buzzer.Device) {

	bzr.BPM = 136

	song := elise()

	for _, val := range song {
		bzr.Tone(val.tone, val.duration)
		time.Sleep(10 * time.Millisecond)
	}
}

type note struct {
	tone     float64
	duration float64
}

// https://www.youtube.com/watch?v=lqFbtmEQbFU
// https://pages.mtu.edu/~suits/notefreqs.html
func elise() []note {

	song := []note{
		{buzzer.E5, buzzer.Eighth},
		{buzzer.Eb5, buzzer.Eighth},
		{buzzer.E5, buzzer.Eighth},
		{buzzer.Eb5, buzzer.Eighth},
		{buzzer.E5, buzzer.Eighth},
		{buzzer.B4, buzzer.Eighth},
		{buzzer.D5, buzzer.Eighth},
		{buzzer.C5, buzzer.Eighth},
		{buzzer.A4, buzzer.Quarter},
		{buzzer.C4, buzzer.Eighth},
		{buzzer.E4, buzzer.Eighth},
		{buzzer.A4, buzzer.Eighth},
		{buzzer.B4, buzzer.Quarter},
		{buzzer.E4, buzzer.Eighth},
		{buzzer.Ab4, buzzer.Eighth},
		{buzzer.B4, buzzer.Eighth},
		{buzzer.C5, buzzer.Quarter},
		{buzzer.E4, buzzer.Eighth},
	}

	song = append(song, song...)

	return song
}
