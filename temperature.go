package main

import (
	"machine"
	"time"
	"tinygo.org/x/drivers/dht"
)

func tempHumidityFn() func() (float32, float32) {

	tempPin := dht.New(machine.GP11, dht.DHT11)

	var (
		latest time.Time
		fTemp  float32
		fHum   float32
	)

	return func() (float32, float32) {
		// too frequent Calls drain the sensor and return zero values
		if time.Since(latest) < time.Second {
			return fTemp, fHum
		}
		temp, hum, _ := tempPin.Measurements()
		latest = time.Now()
		fTemp, fHum = float32(temp)/10, float32(hum)/10

		return fTemp, fHum
	}

}
