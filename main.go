package main

import (
	"fmt"
	"machine"
	"time"
	"tinygo.org/x/drivers/buzzer"
)

func main() {

	btn := machine.GP10
	btn.Configure(machine.PinConfig{
		Mode: machine.PinInput,
	})

	led := machine.LED
	led.Configure(machine.PinConfig{
		Mode: machine.PinOutput,
	})

	// play the song on demand
	btn.SetInterrupt(machine.PinRising|machine.PinFalling, func(p machine.Pin) {
		led.Set(p.Get())
	})

	tempAndHumidity := tempHumidityFn()
	d := NewDisplay()
	for {
		t, h := tempAndHumidity()
		d.Write(
			fmt.Sprintf(
				"Temperature: %02.1f°C\nHumidity: %02.1f%%\n",
				t,
				h,
			),
		)
		time.Sleep(time.Second * 5)
	}

}

func playSong() {
	bzrPin := machine.GP20
	bzrPin.Configure(machine.PinConfig{Mode: machine.PinOutput})
	bzr := buzzer.New(bzrPin)

	play(bzr)
}
