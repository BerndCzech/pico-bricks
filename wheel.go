package main

import (
	"machine"
	"time"
	"tinygo.org/x/drivers/l9110x"
)

func wheel() {
	wheel := l9110x.New(machine.GP21, machine.GP22)
	wheel.Configure()

	println("Forward")
	wheel.Forward()
	wheel.Forward()
	time.Sleep(time.Millisecond * 1000)

	println("Stop")
	wheel.Stop()
	time.Sleep(time.Millisecond * 1000)

	println("Backward")
	wheel.Backward()
	time.Sleep(time.Millisecond * 1000)

	println("Stop")
	wheel.Stop()
	time.Sleep(time.Millisecond * 1000)
}
