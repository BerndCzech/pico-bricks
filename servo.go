package main

import (
	"machine"
	"time"
	"tinygo.org/x/drivers/l293x"
	"tinygo.org/x/drivers/servo"
)

const maxSpeed = 100

func servoTwoPin() {

	err := machine.PWM2.Configure(machine.PWMConfig{
		Period: 16384e3, // 16.384ms
	})
	if err != nil {
		println(err.Error())
		return
	}

	spc, err := machine.PWM2.Channel(machine.GP20)
	if err != nil {
		println(err.Error())
		return
	}

	wheel := l293x.NewWithSpeed(machine.GP21, machine.GP22, spc, machine.PWM2)
	wheel.Configure()

	for i := 0; i <= 10; i++ {
		println("Forward")
		var i uint32
		for i = 0; i < maxSpeed; i += 10 {
			wheel.Forward(i)
			time.Sleep(time.Millisecond * 100)
		}

		println("Stop")
		wheel.Stop()
		time.Sleep(time.Millisecond * 1000)

		println("Backward")
		for i = 0; i < maxSpeed; i += 10 {
			wheel.Backward(i)
			time.Sleep(time.Millisecond * 100)
		}

		println("Stop")
		wheel.Stop()
		time.Sleep(time.Millisecond * 1000)
	}

	println("Stop")
	wheel.Stop()
}

func servoThreePin() {

	pwm := machine.PWM2
	pin := machine.GP21

	s, err := servo.New(pwm, pin)
	if err != nil {
		for {
			println("could not configure servo")
			time.Sleep(time.Second)
		}
		return
	}

	println("setting to 0°")
	s.SetMicroseconds(1000)
	time.Sleep(3 * time.Second)

	println("setting to 45°")
	s.SetMicroseconds(1500)
	time.Sleep(3 * time.Second)

	println("setting to 90°")
	s.SetMicroseconds(2000)
	time.Sleep(3 * time.Second)

}
