package main

import (
	"image/color"
	"machine"
	"tinygo.org/x/drivers/ssd1306"
	"tinygo.org/x/tinyfont"
	"tinygo.org/x/tinyfont/notosans"
)

func NewDisplay() Display {
	machine.I2C0.Configure(machine.I2CConfig{
		Frequency: 400 * machine.KHz,
	})

	display := ssd1306.NewI2C(machine.I2C0)
	display.Configure(ssd1306.Config{
		Address: 0x3C,
		Width:   128,
		Height:  64,
	})

	display.ClearDisplay()
	return Display{
		display: &display,
	}
}

type Display struct {
	display *ssd1306.Device
}

func (d Display) Write(s string) {

	writeText(
		d.display,
		s,
	)
}

func writeText(display *ssd1306.Device, text string) {

	black := color.RGBA{R: 1, G: 1, B: 1, A: 255}

	display.ClearBuffer()
	tinyfont.WriteLine(display, &notosans.Notosans12pt, 0, 24, text, black)
	display.Display()
}
